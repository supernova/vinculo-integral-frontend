/**
 * The default configuration for the app.
 */
export const DEFAULT_API_CONFIG = {
  url: process.env.REACT_APP_API_GATEWAY_ADDRESS || '',
  timeout: 60 * 1000,
  apiPageSize: 50
}
