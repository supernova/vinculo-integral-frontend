import React, { useState } from 'react';

const TagOptions = {
  ESDM: {
    color: '#4169E1',
    description: 'Intervenção naturalista, ensino incidental, motivação e aprendizagem, liderança compartilhada, interação recíproca',
  },
  Escolar: {
    color: '#FFA500',
    description: 'Terapia escolar, Adaptação no ambiente escolar, inclusão nas escolas, avaliação, plano de ensino',
  },
  'Habilidades Sociais': {
    color: '#50C878',
    description: 'Avaliação em habilidades sociais, plano de ensino, mediação com pares, generalização',
  },
  ABA: {
    color: '#DC143C',
    description: 'Análise do comportamento aplicada, ensino por tentativas discretas, ensino estruturado',
  },
};

const sort = (a, b) => a.toString().localeCompare(b.toString())

const SupervisorTags = ({ tags = [], setTags, edit = false }) => {
  const [hoveredTag, setHoveredTag] = useState(null);

  const handleTagChange = React.useCallback((tag) => {
    if (tags.includes(tag)) {
      setTags(tags.filter((t) => t !== tag).sort(sort));
    } else {
      setTags([...tags, tag].sort(sort));
    }
  }, [tags, setTags])

  const handleMouseOver = (tag) => {
    setHoveredTag(tag);
  };

  const Tags = React.useMemo(
    () =>
      Object.keys(TagOptions).filter((key) =>
        edit ? true : tags.indexOf(key) >= 0
      ),
    [tags, edit]
  )

  return (
    <div>
      <label className='label'>Especialidades</label>
      <div className='flex flex-col'>
        <div className='flex flex-row gap-2 overflow-x-auto'>
          {Tags.length === 0 && !edit && <span className='text-xs'>Não há especialidades</span>}
          {Tags.map((category) => (
            <div
              key={category}
              className='flex flex-row items-center relative cursor-default'
            >
              <span
                className='px-2 py-1 rounded-full text-center text-sm'
                style={{
                  backgroundColor: TagOptions[category].color,
                  color: '#FFFFFF'
                }}
                onMouseOver={() => handleMouseOver(category)}
              >
                {category}
              </span>
              {edit && (
                <button
                  className='px-2 py-1 rounded-full hover:bg-gray-200'
                  onClick={() => handleTagChange(category)}
                >
                  {tags.includes(category) ? (
                    <span className='text-red-400'>x</span>
                  ) : (
                    <span className='text-gray-500'>+</span>
                  )}
                </button>
              )}
            </div>
          ))}
        </div>
        <div className='text-sm text-gray-800 mt-2 h-14'>
          {hoveredTag && TagOptions[hoveredTag].description}
        </div>
      </div>
    </div>
  )
};

export default SupervisorTags;
