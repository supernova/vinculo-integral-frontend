import React from 'react'
import { FileUploader } from 'react-drag-drop-files'
import * as XLSX from 'xlsx'
import { useEvent } from '@emerald-works/react-event-bus-client'
import { adminSlice } from '../../reducers'
import { useSelector } from 'react-redux'
import { toast } from 'react-toastify'
import Loading from '../loading'

const ImportUsers = ({ open, onCancel, type, role }) => {
  const [validateUsersList, validatePatientsList, createUser, createPatient] = useEvent([adminSlice.eventBus.validateUsersList, adminSlice.eventBus.validatePatientsList, { ...adminSlice.eventBus.createUser, onError: () => { } }, adminSlice.eventBus.createPatient])
  const usersListErrors = useSelector(adminSlice.selectors.selectUsersListErrors)
  const patientsListErrors = useSelector(adminSlice.selectors.selectPatientsListErrors)
  const [error, setError] = React.useState('')
  const [isValid, setIsValid] = React.useState(false)
  const [isValidated, setIsValidated] = React.useState(false)
  const [invalids, setInvalids] = React.useState([])
  const [objectsToCreate, setObjectsToCreate] = React.useState([])
  const [objectsLength, setObjectsLength] = React.useState(0)
  const [isCreating, setIsCreating] = React.useState(false)
  const person = React.useMemo(() => type === 'user' ? 'usuários' : type === 'patient' ? 'pacientes' : '', [type])
  const disabled = React.useMemo(() => !isValid || !isValidated || isCreating, [isValid, isValidated, isCreating])
  const handleUploadFile = React.useCallback(async (file) => {
    setIsValid(false)
    setIsValidated(false)
    setInvalids([])
    const data = await file.arrayBuffer();
    const workbook = XLSX.read(data);
    const worksheet = workbook.Sheets[workbook.SheetNames[0]];
    const jsonData = XLSX.utils.sheet_to_json(worksheet, {
      header: 1
    })
    const [header, ...rows] = jsonData
    if (type === 'user') {
      const nameIndex = header.indexOf('Nome')
      const emailIndex = header.indexOf('Email')
      if (nameIndex < 0 || emailIndex < 0) {
        setError('Arquivo inválido')
      } else {
        setError('')
        const users = rows.map((row) => ({ name: row[nameIndex], email: row[emailIndex], role })).filter(({ name, email }) => name !== undefined && email !== undefined)
        setObjectsToCreate(users)
        setObjectsLength(users.length)
        validateUsersList.trigger({ users })
      }
    } else if (type === 'patient') {
      const nameIndex = header.indexOf('Nome')
      const docIndex = header.indexOf('CPF')
      if (nameIndex < 0 || docIndex < 0) {
        setError('Arquivo inválido')
      } else {
        setError('')
        const patients = rows.map((row) => ({ name: row[nameIndex], doc: row[docIndex]?.toString() })).filter(({ name, doc }) => name !== undefined && doc !== undefined)
        setObjectsToCreate(patients)
        setObjectsLength(patients.length)
        validatePatientsList.trigger({ patients })
      }
    }
  }, [type])

  const UserTemplate = React.useCallback(() =>
    <div>
      <label className='label'>Seu arquivo, no formato xlsx, deve conter colunas com os cabeçalhos <b>Nome</b> e <b>Email</b>. <a className='underline cursor-pointer' onClick={() => { window.open(window.location.origin + '/modelo_usuarios.xlsx', '_blank') }} download="modelo_usuarios.xlsx">Baixe aqui o modelo</a></label>
    </div>, [])
  const PatientTemplate = React.useCallback(() => <div>
    <label className='label'>Seu arquivo, no formato xlsx, deve conter colunas com os cabeçalhos <b>Nome</b> e <b>CPF</b>. <a className='underline cursor-pointer' onClick={() => { window.open(window.location.origin + '/modelo_pacientes.xlsx', '_blank') }} download="modelo_pacientes.xlsx">Baixe aqui o modelo</a></label>
  </div>, [])

  const validating = React.useMemo(() => validateUsersList.isWorking || validatePatientsList.isWorking, [validateUsersList.isWorking, validatePatientsList.isWorking])

  const create = React.useCallback(() => {
    if (objectsToCreate.length) {
      setIsCreating(true)
      const [object, ...newObjectsToCreate] = objectsToCreate
      setObjectsToCreate(newObjectsToCreate)
      if (type === 'user') {
        createUser.trigger(object)
      } else if (type === 'patient') {
        createPatient.trigger(object)
      }
    } else if (createUser.hasBeenTriggered || createPatient.hasBeenTriggered) {
      toast.success(`${objectsLength} ${person} importados com sucesso!`)
      setIsCreating(false)
      setIsValid(false)
      setIsValidated(false)
    }
  }, [objectsToCreate, type])

  React.useEffect(() => {
    if (!createUser.isWorking && !createPatient.isWorking) {
      create()
    }
  }, [createUser.isWorking, createPatient.isWorking])

  React.useEffect(() => {
    if (type === 'user' && validateUsersList.hasBeenTriggered) {
      setIsValid(usersListErrors.length === 0)
      if (usersListErrors.length > 0) {
        setInvalids(usersListErrors.map(({ user, error }) => `${user.name} - ${user.email}: ${error}`))
      }
      setIsValidated(true)
    } else if (type === 'patient' && validatePatientsList.hasBeenTriggered) {
      setIsValid(patientsListErrors.length === 0)
      if (patientsListErrors.length > 0) {
        setInvalids(
          patientsListErrors.map(
            ({ patient, error }) => `${patient.name} - ${patient.doc}: ${error}`
          )
        )
      }
      setIsValidated(true)
    }
  }, [type, usersListErrors, patientsListErrors])

  const percentage = React.useMemo(() => ((objectsLength - objectsToCreate.length) / objectsLength) * 100, [objectsToCreate, objectsLength])

  return (
    <>
      {open ? (
        <>
          <div className='modal'>
            <div className='relative w-auto my-6 mx-auto max-w-3xl'>
              <div className='modal-content'>
                <div className='flex items-start justify-between p-5 border-b border-solid rounded-t'>
                  <h3 className='title'>Importar {person}</h3>
                  <button
                    className='p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none'
                    onClick={onCancel}
                  >
                    <span className='bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none'>
                      ×
                    </span>
                  </button>
                </div>
                <div className='relative p-2 flex-auto'>
                  <FileUploader
                    handleChange={handleUploadFile}
                    name='file'
                    label='Arraste ou clique para selecionar o arquivo'
                    uploadedLabel={
                      isValid
                        ? 'Selecionado com sucesso'
                        : 'Arraste ou clique para selecionar o arquivo'
                    }
                    hoverTitle='Arraste para cá'
                    types={['xlsx']}
                    disabled={validating}
                  />
                  <div class='w-fullrounded-full h-1.5 my-1'>
                    <div
                      class='bg-primary-blue h-1.5 rounded-full'
                      style={{
                        width:
                          isCreating && objectsLength > 0
                            ? `${percentage}%`
                            : '0%'
                      }}
                    ></div>
                  </div>
                  {isValidated && !isValid && (
                    <div>
                      <p className='label'>
                        Erro na validação de alguns {person}:
                      </p>
                    </div>
                  )}
                  {objectsLength > 0 && isValidated && isValid && (
                    <div>
                      <p className='label'>
                        Arquivo validado com sucesso. Deseja importar{' '}
                        {objectsLength} {person}?
                      </p>
                    </div>
                  )}
                  <div className='h-80 overflow-scroll'>
                    {error.length > 0 && (
                      <label className='label'>Erro: {error}</label>
                    )}
                    {validating && (
                      <div className='flex justify-center w-full gap-2'>
                        <p className='text-sm font-medium text-gray-900'>
                          Validando arquivo
                        </p>{' '}
                        <Loading size={5} />
                      </div>
                    )}
                    {!isValid && (
                      <div>
                        {invalids.map((invalid) => (
                          <p className='label'>{invalid}</p>
                        ))}
                      </div>
                    )}
                  </div>
                  {type === 'user' ? (
                    <UserTemplate />
                  ) : type === 'patient' ? (
                    <PatientTemplate />
                  ) : (
                    <></>
                  )}
                </div>
                <div className='flex items-center justify-end rounded-b px-4'>
                  <button
                    className='btn-secondary'
                    type='button'
                    onClick={onCancel}
                  >
                    Fechar
                  </button>
                  <button
                    className={`btn-primary ${disabled ? 'btn-disabled' : ''}`}
                    type='button'
                    onClick={create}
                    disabled={disabled}
                  >
                    Importar
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className='opacity-25 fixed inset-0 z-40 bg-black' />
        </>
      ) : null}
    </>
  )
}
export default ImportUsers
