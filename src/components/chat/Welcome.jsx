import React from 'react'

export default function Welcome () {
  return (
    <div className='col-span-2 block bg-white'>
      <div className='pl-5'>
        <div className='text-center'>
          <h2 className='text-xl text-gray-500 p-10'>
            Selecione um grupo
          </h2>
        </div>
      </div>
    </div>
  )
}
