
import { createTheme, responsiveFontSizes } from '@material-ui/core'

export default function createCustomTheme (options) {
  const theme = createTheme({
    palette: {
      type: options?.darkTheme ? 'dark' : 'light'
    },
    overrides: {
      MuiListItemText: {
        primary: {
          fontSize: '12px'
        }
      },
      MuiToolbar: {
        root: {
          backgroundColor: '#303030'
        }
      }
    }
  }, {
    palette: {
      primary: {
        main: '#FBC337'
      }
    }
  })

  return responsiveFontSizes(theme)
}
