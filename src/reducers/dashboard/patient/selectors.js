import name from './name'

const selectSliceData = (state) => state[name]
export const selectPatients = (state) => selectSliceData(state).patients
export const selectSearchPatients = (state) =>
  selectSliceData(state).searchPatients
export const selectPatient = (state) => selectSliceData(state).patient
