import name from './name'

const selectSliceData = (state) => state[name]

export const selectChatMembers = (state) => selectSliceData(state).chatMembers
export const selectPatient = (state) => selectSliceData(state).patient
export const selectPatients = (state) => selectSliceData(state).patients
export const selectSearchPatients = (state) =>
  selectSliceData(state).searchPatients
export const selectIsSearching = (state) => selectSliceData(state).isSearching
export const selectIsWorking = (state) => selectSliceData(state).isWorking
