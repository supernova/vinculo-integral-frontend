export const resetStore = (state, initialState) => {
  for (const key of Object.keys(initialState)) {
    state[key] = initialState[key]
  }
}
