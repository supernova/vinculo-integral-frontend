import name from './name'

const selectSliceData = (state) => state[name]

export const selectMessages = (state) => selectSliceData(state).messages
export const selectHasMoreMessages = (state) =>
  selectSliceData(state).hasMoreMessages
export const selectChatMembers = (state) => selectSliceData(state).chatMembers
