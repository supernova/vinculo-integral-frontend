import name from './name'

const selectSliceData = state => state[name]

const selectTenant = state => selectSliceData(state).tenant

export {
  selectTenant
}
