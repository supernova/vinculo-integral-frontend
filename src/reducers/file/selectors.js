import name from './name'

const selectSliceData = (state) => state[name]

const selectFiles = (state) => selectSliceData(state).files
const selectUploadFiles = (state) => selectSliceData(state).uploadFiles
export { selectFiles, selectUploadFiles }
