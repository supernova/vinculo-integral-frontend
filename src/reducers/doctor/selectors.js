import name from './name'

const selectSliceData = (state) => state[name]
const selectSearchDoctors = (state) => selectSliceData(state).searchDoctors

export { selectSearchDoctors }
