import name from './name'

const selectSliceData = (state) => state[name]

const selectSearchPatients = (state) => selectSliceData(state).searchPatients
const selectPatient = (state) => selectSliceData(state).patient

export { selectSearchPatients, selectPatient }
