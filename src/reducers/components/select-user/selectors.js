import name from './name'

const selectSliceData = (state) => state[name]
const selectSearchUsers = (state) => selectSliceData(state).searchUsers
const selectUser = (state) => selectSliceData(state).user
const selectUsers = (state) => selectSliceData(state).users

export { selectSearchUsers, selectUser, selectUsers }
