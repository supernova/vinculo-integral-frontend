import * as actions from './slice'

export const searchUsers = {
  eventName: 'getUsers',
  onSuccess: {
    action: actions.setSearchUsers,
    redux: true
  }
}