import name from './name'

const selectSliceData = (state) => state[name]
const selectSearchDoctors = (state) => selectSliceData(state).searchDoctors
const selectDoctor = (state) => selectSliceData(state).doctor
const selectPatientDoctors = (state) => selectSliceData(state).patientDoctors

export { selectSearchDoctors, selectDoctor, selectPatientDoctors }
