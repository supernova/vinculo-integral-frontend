import * as actions from './slice'

export const searchSupervisors = {
  eventName: 'getSupervisors',
  onSuccess: {
    action: actions.setSearchSupervisors,
    redux: true
  }
}
