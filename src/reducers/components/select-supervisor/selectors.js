import name from './name'

const selectSliceData = (state) => state[name]
const selectSupervisor = (state) => selectSliceData(state).supervisor
const selectSearchSupervisors = (state) =>
  selectSliceData(state).searchSupervisors

export { selectSupervisor, selectSearchSupervisors }
