import reducer, * as actions from './slice'
import * as selectors from './selectors'
import name from './name'
import * as eventBus from './event-bus'

export default {
  reducer,
  actions,
  selectors,
  name,
  eventBus
}
