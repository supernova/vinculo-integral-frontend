import name from './name'

const selectSliceData = (state) => state[name]

const selectFamilyUsers = (state) => selectSliceData(state).familyUsers
const selectFamilyUser = (state) => selectSliceData(state).familyUser
const selectPatientFamily = (state) => selectSliceData(state).patientFamily

export { selectFamilyUsers, selectFamilyUser, selectPatientFamily }
