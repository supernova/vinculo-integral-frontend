import coreSlice from './core'
import tenantSlice from './tenant'
import usersSlice from './users'
import adminSlice from './admin'
import chatSlice from './chat'
import patientSlice from './patient'
import doctorSlice from './doctor'
import componentsSelectFamilySlice from './components/select-family'
import componentsSelectDoctorSlice from './components/select-doctor'
import componentsSelectPatientSlice from './components/select-patient'
import componentsSelectUserSlice from './components/select-user'
import dashboardAgendaSlice from './dashboard/agenda'
import dashboardChatSlice from './dashboard/chat'
import dashboardPatientSlice from './dashboard/patient'
import componentSearchPatientsSlice from './components/search-patients'
import dashboardAttendanceSlice from './dashboard/attendance'
import meetingSlice from './meeting'
import fileSlice from './file'
import componentsSelectSupervisorSlice from './components/select-supervisor'

export {
  coreSlice,
  patientSlice,
  doctorSlice,
  chatSlice,
  tenantSlice,
  usersSlice,
  adminSlice,
  componentsSelectFamilySlice,
  componentsSelectDoctorSlice,
  componentsSelectPatientSlice,
  componentsSelectUserSlice,
  dashboardAgendaSlice,
  dashboardChatSlice,
  dashboardPatientSlice,
  componentSearchPatientsSlice,
  dashboardAttendanceSlice,
  meetingSlice,
  fileSlice,
  componentsSelectSupervisorSlice
}
