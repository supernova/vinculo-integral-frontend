import React, { useState, createContext } from 'react'
import { ThemeProvider } from '@material-tailwind/react'

const initialState = true

export const CustomThemeContext = createContext(initialState)

export function CustomThemeProvider ({ children }) {
  const [isDarkTheme, setIsDarkTheme] = useState(initialState)

  return (
    <CustomThemeContext.Provider value={{ isDarkTheme, setIsDarkTheme }}>
      <ThemeProvider>
        {children}
      </ThemeProvider>
    </CustomThemeContext.Provider>
  )
}
