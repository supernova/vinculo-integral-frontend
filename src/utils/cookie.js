export const setCookie = (cookie) => {
  document.cookie = cookie
}

export const setCookies = (cookies) => cookies.forEach(setCookie)
