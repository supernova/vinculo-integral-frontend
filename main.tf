terraform {
  backend "s3" {
    bucket         = "ew-nova-terraform-state"
    region         = "eu-west-2"
    key            = "react.tfstate"
    dynamodb_table = "ew-nova-terraform-state-lock"
  }
}

variable "name" {}

variable "subdomain" {}

variable "app" {}

variable "tld" {}

variable "review" {}

variable "live" {}

locals {
  fqdn         = "${var.subdomain}${var.tld}"
  s3_origin_id = "ew-${var.name}"
  s3_bucket    = "ew-${var.name}"
  owner        = "EmeraldWorks"
}

provider "aws" {
  region = "us-east-1"
}

data "aws_route53_zone" "tld" {
  name = "${var.tld}"
}

resource "aws_acm_certificate" "cert" {
  count                     = "${var.live}"
  domain_name               = "${var.app}.${local.fqdn}"
  validation_method         = "DNS"
  subject_alternative_names = ["*.${var.app}.${local.fqdn}"]

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_acm_certificate" "cert" {
  depends_on = ["aws_acm_certificate.cert"]
  domain     = "${local.fqdn}"
  statuses   = ["ISSUED"]
}

resource "aws_route53_record" "cert_validation" {
  count   = "${var.live}"
  name    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_name}"
  type    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_type}"
  zone_id = "${data.aws_route53_zone.tld.id}"
  records = ["${aws_acm_certificate.cert.domain_validation_options.0.resource_record_value}"]
  ttl     = 60

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "cert" {
  count           = "${var.live}"
  certificate_arn = "${aws_acm_certificate.cert.arn}"

  validation_record_fqdns = ["${aws_acm_certificate.cert.domain_validation_options.0.resource_record_name}",
    "${aws_acm_certificate.cert.domain_validation_options.1.resource_record_name}",
  ]

  depends_on = ["aws_route53_record.cert_validation"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_s3_bucket" "bucket" {
  bucket        = "${local.s3_bucket}"
  force_destroy = true
  depends_on    = ["aws_acm_certificate_validation.cert"]

  website {
    index_document = "index.html"
    error_document = "index.html"
  }

  tags = {
    Name        = "${var.name}"
    Environment = "${local.fqdn}"
    Creator     = "marcelomarra"
    Owner       = "${local.owner}"
    Terraform   = "true"
  }
}

# resource "aws_route53_record" "record" {
#   zone_id = "${data.aws_route53_zone.tld.zone_id}"
#   name    = "${local.fqdn}"
#   type    = "A"

#   alias {
#     name                   = "${aws_cloudfront_distribution.distribution.domain_name}"
#     zone_id                = "${aws_cloudfront_distribution.distribution.hosted_zone_id}"
#     evaluate_target_health = true
#   }
# }

resource "aws_route53_record" "wildcard" {
  zone_id = "${data.aws_route53_zone.tld.zone_id}"
  name    = "${var.app}.${local.fqdn}"
  type    = "A"

  alias {
    name                   = "${aws_cloudfront_distribution.distribution.domain_name}"
    zone_id                = "${aws_cloudfront_distribution.distribution.hosted_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_cloudfront_distribution" "distribution" {
  depends_on          = ["aws_s3_bucket.bucket"]
  aliases             = ["${var.app}.${local.fqdn}"]
  enabled             = true
  comment             = "${var.app}.${local.fqdn} CloudFront"
  default_root_object = "/index.html"
  wait_for_deployment = false

  origin {
    domain_name = "${aws_s3_bucket.bucket.website_endpoint}"
    origin_id   = "${local.s3_origin_id}"

    custom_origin_config {
      origin_protocol_policy = "http-only"
      http_port              = 80
      https_port             = 443
      origin_ssl_protocols   = ["TLSv1.2", "TLSv1.1", "TLSv1"]
    }
  }

  custom_error_response {
    error_code         = 400
    response_code      = 200
    response_page_path = "/index.html"
  }

  custom_error_response {
    error_code         = 403
    response_code      = 200
    response_page_path = "/index.html"
  }

  custom_error_response {
    error_code         = 404
    response_code      = 200
    response_page_path = "/index.html"
  }

  custom_error_response {
    error_code         = 405
    response_code      = 200
    response_page_path = "/index.html"
  }

  custom_error_response {
    error_code         = 414
    response_code      = 200
    response_page_path = "/index.html"
  }

  custom_error_response {
    error_code         = 416
    response_code      = 200
    response_page_path = "/index.html"
  }

  custom_error_response {
    error_code         = 500
    response_code      = 200
    response_page_path = "/index.html"
  }

  custom_error_response {
    error_code         = 501
    response_code      = 200
    response_page_path = "/index.html"
  }

  custom_error_response {
    error_code         = 502
    response_code      = 200
    response_page_path = "/index.html"
  }

  custom_error_response {
    error_code         = 503
    response_code      = 200
    response_page_path = "/index.html"
  }

  custom_error_response {
    error_code         = 504
    response_code      = 200
    response_page_path = "/index.html"
  }

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "${local.s3_origin_id}"
    compress         = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
  }

  lifecycle {
    create_before_destroy = true
    ignore_changes        = ["origin"]
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Name        = "${var.name}"
    Environment = "${local.fqdn}"
    Creator     = "marcelomarra"
    Owner       = "${local.owner}"
    Terraform   = "true"
  }

  viewer_certificate {
    acm_certificate_arn = "${data.aws_acm_certificate.cert.arn}"
    ssl_support_method  = "sni-only"
  }
}

output "cloudfront_id" {
  value = "${aws_cloudfront_distribution.distribution.id}"
}
