const { Stack } = require('aws-cdk-lib')
const cdk = require('aws-cdk-lib')
const cloudfront = require('aws-cdk-lib/aws-cloudfront')
const origins = require('aws-cdk-lib/aws-cloudfront-origins')
const acm = require('aws-cdk-lib/aws-certificatemanager')
const s3 = require('aws-cdk-lib/aws-s3')
const s3deploy = require('aws-cdk-lib/aws-s3-deployment')
const route53 = require('aws-cdk-lib/aws-route53')
const targets = require('aws-cdk-lib/aws-route53-targets')
const { CachePolicy } = require('aws-cdk-lib/aws-cloudfront')

module.exports = {
  FrontendStack: class FrontendStack extends Stack {
    constructor(
      scope,
      id,
      props,
      tld = '',
      projectName = '',
      domainName = '',
      stage = 'default',
      buildPath = 'dist',
      errorDocument = '/index.html'
    ) {
      super(scope, id, props)

      // Create a S3 bucket and upload dist folder to it
      const bucket = new s3.Bucket(this, 'Bucket', {
        bucketName: `vi-frontend-${stage}.${projectName}.${tld}`,
        publicReadAccess: false,
        removalPolicy: cdk.RemovalPolicy.DESTROY,
        blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
        accessControl: s3.BucketAccessControl.PRIVATE,
        objectOwnership: s3.ObjectOwnership.BUCKET_OWNER_ENFORCED,
        encryption: s3.BucketEncryption.S3_MANAGED,
        autoDeleteObjects: true
      })

      // Output the bucket domain name
      new cdk.CfnOutput(this, 'BucketDomainName', {
        value: bucket.bucketDomainName
      })

      // // TODO add a status page as the single and only page
      const origin = new origins.S3Origin(bucket)

      // request a certificate for the domain
      const hostedZone = route53.HostedZone.fromLookup(this, 'HostedZone', {
        domainName: tld
      })

      const certificate = new acm.DnsValidatedCertificate(this, 'Certificate', {
        domainName,
        hostedZone,
        region: 'us-east-1'
      })
      const distribution = new cloudfront.Distribution(this, 'Distribution', {
        defaultBehavior: {
          origin,
          viewerProtocolPolicy:
            cloudfront.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          compress: true,
          cachePolicy: CachePolicy.CACHING_OPTIMIZED
        },
        comment: `Distribution for ${bucket.bucketName}`,
        enableLogging: true,
        logIncludesCookies: true,
        domainNames: [domainName],
        certificate,
        errorResponses: [
          {
            httpStatus: 404,
            responseHttpStatus: 200,
            responsePagePath: errorDocument
          },
          {
            httpStatus: 403,
            responseHttpStatus: 200,
            responsePagePath: errorDocument
          }
        ]
      })
      // Upload the dist folder to the bucket
      new s3deploy.BucketDeployment(this, 'DeployWebsite', {
        sources: [s3deploy.Source.asset(buildPath)],
        destinationBucket: bucket,
        distribution
      })

      const recordTarget = route53.RecordTarget.fromAlias(
        new targets.CloudFrontTarget(distribution)
      )

      new route53.ARecord(this, 'SiteARecord', {
        recordName: domainName,
        target: recordTarget,
        zone: hostedZone
      })

      new route53.AaaaRecord(this, 'SiteAAAARecord', {
        recordName: domainName,
        target: recordTarget,
        zone: hostedZone
      })

      new cdk.CfnOutput(this, 'DomainName', {
        value: domainName
      })
    }
  }
}
