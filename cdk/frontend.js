const cdk = require('aws-cdk-lib')
const { FrontendStack } = require('./frontend-stack')
const { kebabCase } = require('lodash')

async function main () {
  const app = new cdk.App()

  const tld = process.env.TLD
  const projectName = 'vi-frontend'
  const stage = process.env.STAGE
  const buildPath = './build'
  const errorDocument = '/index.html'
  const region = 'sa-east-1'

  if (!tld) {
    throw new Error('Env var TLD is required.')
  }

  if (!projectName) {
    throw new Error('Env var PROJECT_NAME is required.')
  }

  const domainName = stage ? `${stage}.${tld}` : `${tld}`

  const stackName = stage ? `${stage}-${projectName}-${kebabCase(tld)}` : `${projectName}-${kebabCase(tld)}`

  const infra = new FrontendStack(app, stackName, {
    env: {
      account: process.env.CDK_DEFAULT_ACCOUNT,
      region
    }
  }, tld, projectName, domainName, stage, buildPath, errorDocument)

  if (!infra) {
    throw new Error('Infra failed to synth')
  }
}

main()
