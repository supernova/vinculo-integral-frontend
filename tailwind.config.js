/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'primary-gray': '#F9FAFB',
        'secondary-gray': '#7F7F7F',
        'primary-blue': '#1C64F2',
        'secondary-blue': '#1A56DB',
        'light-gray': '#EFF4FB',
        'border-gray': '#0000003b'
      },
      fontFamily: {
        inter: ['Inter', 'sans-serif']
      },
      transitionProperty: {
        height: 'height'
      }
    }
  },
  plugins: []
}